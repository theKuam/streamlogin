package hainm18.example.streamlogin.state

sealed class LoginUiState {
    companion object {
        const val SUCCESS = "Success"
        const val LOADING = "Loading"
        const val EMPTY = "Empty"
        const val ERROR = "Error"
        const val UNDEFINED = "Undefined"
    }

    object Success : LoginUiState()
    object Loading : LoginUiState()
    object Empty : LoginUiState()
    data class Error(val message: String) : LoginUiState()
}
