package hainm18.example.streamlogin.ui.view

import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import hainm18.example.streamlogin.databinding.FragmentStreamValueBinding
import hainm18.example.streamlogin.state.LoginUiState
import hainm18.example.streamlogin.ui.base.BaseFragment
import hainm18.example.streamlogin.viewmodel.MainViewModel
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class StreamValueFragment : BaseFragment<FragmentStreamValueBinding>() {
    private val mainViewModel: MainViewModel by activityViewModels()

    override fun getViewBinding(): FragmentStreamValueBinding =
        FragmentStreamValueBinding.inflate(layoutInflater)

    override fun initAction() {
        super.initAction()
        observeLiveData()
        observeSharedFlow()
        observeStateFlow()
    }

    private fun observeLiveData() = with(binding) {
        val resultObserver = Observer<LoginUiState> { state ->
            liveDataValue.text = state.toString()
        }

        mainViewModel.liveDataLoginState.observe(viewLifecycleOwner, resultObserver)
    }

    private fun observeSharedFlow() = with(binding) {
        lifecycleScope.launchWhenStarted {
            mainViewModel.sharedFlowLoginState.collect { state ->
                sharedFlowValue.text = state.toString()
            }
        }
    }

    private fun observeStateFlow() = with(binding) {
        lifecycleScope.launchWhenStarted {
            mainViewModel.stateFlowLogInState.collect { state ->
                stateFlowValue.text = state.toString()
            }
        }
    }
}