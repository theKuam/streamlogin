package hainm18.example.streamlogin.ui.view

import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import hainm18.example.streamlogin.R
import hainm18.example.streamlogin.databinding.FragmentLiveDataBinding
import hainm18.example.streamlogin.state.LoginUiState
import hainm18.example.streamlogin.ui.base.BaseFragment
import hainm18.example.streamlogin.viewmodel.MainViewModel

@AndroidEntryPoint
class LiveDataFragment : BaseFragment<FragmentLiveDataBinding>() {
    private val mainViewModel: MainViewModel by activityViewModels()
    override fun getViewBinding(): FragmentLiveDataBinding =
        FragmentLiveDataBinding.inflate(layoutInflater)

    override fun initAction() = with(binding) {
        super.initAction()
        btnLogin.setOnClickListener {
            mainViewModel.liveDataLogin(
                usernameField.text.toString(),
                passwordField.text.toString()
            )
        }
        val resultObserver = Observer<LoginUiState> { state ->
            when (state) {
                is LoginUiState.Loading -> {
                    progressBar.visibility = View.VISIBLE
                }
                is LoginUiState.Success -> {
                    Toast.makeText(
                        requireContext(),
                        "Successfully logged in",
                        Toast.LENGTH_LONG
                    ).show()
                    progressBar.visibility = View.INVISIBLE
                    findNavController().navigate(R.id.action_liveDataFragment_to_loggedInFragment)

                }
                is LoginUiState.Error -> {
                    Toast.makeText(requireContext(), state.message, Toast.LENGTH_LONG).show()
                    progressBar.visibility = View.INVISIBLE
                }
                else -> Unit
            }
        }

        mainViewModel.liveDataLoginState.observe(viewLifecycleOwner, resultObserver)
    }
}