package hainm18.example.streamlogin.ui.view

import android.view.View
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import hainm18.example.streamlogin.R
import hainm18.example.streamlogin.databinding.FragmentSharedFlowBinding
import hainm18.example.streamlogin.state.LoginUiState
import hainm18.example.streamlogin.ui.base.BaseFragment
import hainm18.example.streamlogin.viewmodel.MainViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class SharedFlowFragment : BaseFragment<FragmentSharedFlowBinding>() {
    private val mainViewModel: MainViewModel by activityViewModels()

    override fun getViewBinding(): FragmentSharedFlowBinding =
        FragmentSharedFlowBinding.inflate(layoutInflater)

    override fun initAction(): Unit = with(binding) {
        super.initAction()
        btnLogin.setOnClickListener {
            mainViewModel.sharedFlowLogin(
                usernameField.text.toString(),
                passwordField.text.toString()
            )
        }
        lifecycleScope.launchWhenStarted {
            mainViewModel.sharedFlowLoginState.collect { state ->
                when (state) {
                    is LoginUiState.Loading -> {
                        progressBar.visibility = View.VISIBLE
                    }
                    is LoginUiState.Success -> {
                        Toast.makeText(
                            requireContext(),
                            "Successfully logged in",
                            Toast.LENGTH_LONG
                        ).show()
                        progressBar.visibility = View.INVISIBLE
                        findNavController().navigate(R.id.action_sharedFlowFragment_to_loggedInFragment)

                    }
                    is LoginUiState.Error -> {
                        Toast.makeText(requireContext(), state.message, Toast.LENGTH_LONG).show()
                        progressBar.visibility = View.INVISIBLE
                    }
                    else -> Unit
                }
            }
        }
    }
}