package hainm18.example.streamlogin.ui.view

import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import hainm18.example.streamlogin.R
import hainm18.example.streamlogin.databinding.FragmentLoggedInBinding
import hainm18.example.streamlogin.ui.base.BaseFragment
import hainm18.example.streamlogin.viewmodel.MainViewModel

@AndroidEntryPoint
class LoggedInFragment : BaseFragment<FragmentLoggedInBinding>() {
    private val mainViewModel: MainViewModel by activityViewModels()
    override fun getViewBinding(): FragmentLoggedInBinding =
        FragmentLoggedInBinding.inflate(layoutInflater)

    override fun initAction() = with(binding) {
        super.initAction()
        btnLogout.setOnClickListener {
            val previousFragmentId =
                findNavController().previousBackStackEntry?.destination?.id ?: -1
            mainViewModel.logout(previousFragmentId)
            findNavController().navigate(R.id.action_loggedInFragment_to_mainFragment)
        }
    }
}