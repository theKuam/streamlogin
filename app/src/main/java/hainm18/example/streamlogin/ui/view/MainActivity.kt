package hainm18.example.streamlogin.ui.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint
import hainm18.example.streamlogin.R
import hainm18.example.streamlogin.databinding.ActivityMainBinding

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        initView()
    }

    private fun initView() = with(binding) {
        val view = root
        supportFragmentManager.beginTransaction()
            .add(R.id.fragment_stream_value, StreamValueFragment())
            .commit()
        setContentView(view)
    }
}