package hainm18.example.streamlogin.ui.view

import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import hainm18.example.streamlogin.R
import hainm18.example.streamlogin.databinding.FragmentMainBinding
import hainm18.example.streamlogin.ui.base.BaseFragment

@AndroidEntryPoint
class MainFragment : BaseFragment<FragmentMainBinding>() {

    override fun getViewBinding(): FragmentMainBinding = FragmentMainBinding.inflate(layoutInflater)

    override fun initAction() = with(binding) {
        super.initAction()
        btnLiveData.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_liveDataFragment)
        }
        btnSharedFlow.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_sharedFlowFragment)
        }
        btnStateFlow.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_stateFlowFragment)
        }
    }
}