package hainm18.example.streamlogin

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class StreamLoginApplication : Application()