package hainm18.example.streamlogin.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import hainm18.example.streamlogin.R
import hainm18.example.streamlogin.state.LoginUiState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor() : ViewModel() {
    companion object {
        const val FAKE_NETWORK_REQUEST = 2000L
        const val DEFAULT_USERNAME = "hainm18"
        const val DEFAULT_PASSWORD = "123456"
        const val ERROR_MESSAGE = "Incorrect username or password."
    }

    private val _liveDataLoginState = MutableLiveData<LoginUiState>()
    val liveDataLoginState = _liveDataLoginState

    private val _sharedFlowLoginState = MutableSharedFlow<LoginUiState>(
        replay = 1,
        onBufferOverflow = BufferOverflow.DROP_OLDEST
    )
    val sharedFlowLoginState = _sharedFlowLoginState.asSharedFlow()

    private val _stateFlowLoginState = MutableStateFlow<LoginUiState>(LoginUiState.Empty)
    val stateFlowLogInState = _stateFlowLoginState.asStateFlow()

    fun stateFlowLogin(username: String, password: String) = viewModelScope.launch(Dispatchers.IO) {
        _stateFlowLoginState.value = LoginUiState.Loading
        delay(FAKE_NETWORK_REQUEST)
        if (username == DEFAULT_USERNAME && password == DEFAULT_PASSWORD) {
            _stateFlowLoginState.value = LoginUiState.Success
        } else {
            _stateFlowLoginState.value = LoginUiState.Error(ERROR_MESSAGE)
        }
    }
//    {
//        _stateFlowLoginState.value = LoginUiState.Loading
//        sleep(FAKE_NETWORK_REQUEST)
//        if (username == DEFAULT_USERNAME && password == DEFAULT_PASSWORD) {
//            _stateFlowLoginState.value = LoginUiState.Success
//        }
//        else {
//            _stateFlowLoginState.value = LoginUiState.Error(ERROR_MESSAGE)
//        }
//    }

    fun sharedFlowLogin(username: String, password: String) =
        viewModelScope.launch(Dispatchers.IO) {
            _sharedFlowLoginState.emit(LoginUiState.Loading)
            delay(FAKE_NETWORK_REQUEST)
            if (username == DEFAULT_USERNAME && password == DEFAULT_PASSWORD) {
                _sharedFlowLoginState.emit(LoginUiState.Success)
            } else {
                _sharedFlowLoginState.emit(LoginUiState.Error(ERROR_MESSAGE))
            }
        }

    fun liveDataLogin(username: String, password: String) = viewModelScope.launch(Dispatchers.IO) {
        _liveDataLoginState.postValue(LoginUiState.Loading)
        delay(FAKE_NETWORK_REQUEST)
        if (username == DEFAULT_USERNAME && password == DEFAULT_PASSWORD) {
            _liveDataLoginState.postValue(LoginUiState.Success)
        } else {
            _liveDataLoginState.postValue(LoginUiState.Error(ERROR_MESSAGE))
        }
    }

    fun logout(prevFragmentId: Int) = viewModelScope.launch(Dispatchers.IO) {
        when (prevFragmentId) {
            R.id.liveDataFragment -> _liveDataLoginState.postValue(LoginUiState.Empty)
            R.id.sharedFlowFragment -> _sharedFlowLoginState.emit(LoginUiState.Empty)
            R.id.stateFlowFragment -> _stateFlowLoginState.value = LoginUiState.Empty
            else -> Unit
        }
    }
//    {
//        _liveDataLoginState.postValue(LoginUiState.Loading)
//        if (username == DEFAULT_USERNAME && password == DEFAULT_PASSWORD) {
//            _liveDataLoginState.postValue(LoginUiState.Success)
//        }
//        else {
//            _liveDataLoginState.postValue(LoginUiState.Error(ERROR_MESSAGE))
//        }
//    }
}
